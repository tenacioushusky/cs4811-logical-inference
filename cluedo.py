'''cluedo.py - project skeleton for a propositional reasoner
for the game of Clue.  Unimplemented portions have the comment "TO
BE IMPLEMENTED AS AN EXERCISE".  The reasoner does not include
knowledge of how many cards each player holds.
Originally by Todd Neller
Ported to Python by Dave Musicant
Adapted to course needs by Laura Brown

Copyright (C) 2008 Dave Musicant

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Information about the GNU General Public License is available online at:
  http://www.gnu.org/licenses/
To receive a copy of the GNU General Public License, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.'''

import cnf


class Cluedo:
    suspects = ['sc', 'mu', 'wh', 'gr', 'pe', 'pl']
    weapons  = ['kn', 'cs', 're', 'ro', 'pi', 'wr']
    rooms    = ['ha', 'lo', 'di', 'ki', 'ba', 'co', 'bi', 'li', 'st']
    casefile = "cf"
    hands    = suspects + [casefile]
    cards    = suspects + weapons + rooms

    """
    Return ID for player/card pair from player/card indicies
    """
    @staticmethod
    def getIdentifierFromIndicies(hand, card):
        return hand * len(Cluedo.cards) + card + 1

    """
    Return ID for player/card pair from player/card names
    """
    @staticmethod
    def getIdentifierFromNames(hand, card):
        return Cluedo.getIdentifierFromIndicies(Cluedo.hands.index(hand), Cluedo.cards.index(card))


def deal(hand, cards):
    "Construct the CNF clauses for the given cards being in the specified hand"
    "*** YOUR CODE HERE ***"
    pl_card1 = Cluedo.getIdentifierFromNames(hand, cards[0])
    pl_card2 = Cluedo.getIdentifierFromNames(hand, cards[1])
    pl_card3 = Cluedo.getIdentifierFromNames(hand, cards[2])
    return [[pl_card1],[pl_card2],[pl_card3]]
    

def axiom_card_exists():
    """
    Construct the CNF clauses which represents:
        'Each card is in at least one place'
    """
    "*** YOUR CODE HERE ***"
    hand_list=[]
    card_list=[]
    j=x=0
    
    for card in Cluedo.cards:
        for hand in Cluedo.hands:
            card_list.append(Cluedo.getIdentifierFromNames(hand, card))
        hand_list.append(card_list)
        card_list=[]

    return hand_list


def axiom_card_unique():
    """
    Construct the CNF clauses which represents:
        'If a card is in one place, it can not be in another place'
    """
    "*** YOUR CODE HERE ***"
    hand_list=[]
    card_list=[]
    
    #for card1 in Cluedo.cards:
    #    for card2 in Cluedo.cards:
    #        for hand in Cluedo.hands:
    #          card_list.append(-Cluedo.getIdentifierFromNames(hand,card1))
    #          card_list.append(-Cluedo.getIdentifierFromNames(hand,card2))
    #          hand_list.append(card_list)
    #          card_list=[]

    for card1 in Cluedo.cards:
        for hand in Cluedo.hands:
            for card2 in Cluedo.cards:
                card_list.append(-Cluedo.getIdentifierFromNames(hand, card1))
                card_list.append(-Cluedo.getIdentifierFromNames(hand, card2))
                hand_list.append(card_list)
                card_list=[]

    return hand_list


def axiom_casefile_exists():
    """
    Construct the CNF clauses which represents:
        'At least one card of each category is in the case file'
    """
    "*** YOUR CODE HERE ***"
    hand_list=[]
    card_list=[]
    for card in Cluedo.suspects:
        card_list.append(Cluedo.getIdentifierFromNames('cf',card))
    hand_list.append(card_list)
    card_list=[]

    for card in Cluedo.weapons:
        card_list.append(Cluedo.getIdentifierFromNames('cf',card))
    hand_list.append(card_list)
    card_list=[]

    for card in Cluedo.rooms:
        card_list.append(Cluedo.getIdentifierFromNames('cf',card))
    hand_list.append(card_list)
    card_list=[]
            
    return hand_list


def axiom_casefile_unique():
    """
    Construct the CNF clauses which represents:
        'No two cards in each category are in the case file'
    """
    "*** YOUR CODE HERE ***"
    hand_list=[]
    card_list=[]
    for card1 in Cluedo.suspects:
        for card2 in Cluedo.suspects:
            card_list.append(-Cluedo.getIdentifierFromNames('cf',card1))
            card_list.append(-Cluedo.getIdentifierFromNames('cf',card2))
            hand_list.append(card_list)
            card_list=[]

    for card1 in Cluedo.weapons:
        for card2 in Cluedo.weapons:
            card_list.append(-Cluedo.getIdentifierFromNames('cf',card1))
            card_list.append(-Cluedo.getIdentifierFromNames('cf',card2))
            hand_list.append(card_list)
            card_list=[]

    for card1 in Cluedo.rooms:
        for card2 in Cluedo.rooms:
            card_list.append(-Cluedo.getIdentifierFromNames('cf',card1))
            card_list.append(-Cluedo.getIdentifierFromNames('cf',card2))
            hand_list.append(card_list)
            card_list=[]
    
    print hand_list        
    return hand_list


def suggest(suggester, card1, card2, card3, refuter, cardShown):
    "Construct the CNF clauses representing facts and/or clauses learned from a suggestion"
    "*** YOUR CODE HERE ***"

    knowledge_list=[]
    if refuter is None:
        for player in Cluedo.suspects:
            if player is not suggester:
                knowledge_list.append([(-1)*Cluedo.getIdentifierFromNames(player, card1)]) #The other players know I have these cards
                knowledge_list.append([(-1)*Cluedo.getIdentifierFromNames(player, card2)])
                knowledge_list.append([(-1)*Cluedo.getIdentifierFromNames(player, card3)])
    else:
        #Give me the player clockwise to me
        player_index = (Cluedo.suspects.index(suggester) + 1)%len(Cluedo.suspects)
        playerCharacter = Cluedo.suspects[player_index]
        while player_index is not Cluedo.suspects.index(refuter):
            playerCharacter = Cluedo.suspects[player_index]
            knowledge_list.append([(-1)*Cluedo.getIdentifierFromNames(playerCharacter, card1)]) #These players don't have cards to refute me
            knowledge_list.append([(-1)*Cluedo.getIdentifierFromNames(playerCharacter, card2)])
            knowledge_list.append([(-1)*Cluedo.getIdentifierFromNames(playerCharacter, card3)])
            player_index = (player_index + 1)%len(Cluedo.suspects) #Give me the next player


        if cardShown is None:
            playerCharacter = Cluedo.suspects[player_index]
            rf_card1 = Cluedo.getIdentifierFromNames(playerCharacter, card1)
            rf_card2 = Cluedo.getIdentifierFromNames(playerCharacter, card2)
            rf_card3 = Cluedo.getIdentifierFromNames(playerCharacter, card3)
            knowledge_list.append([rf_card1, rf_card2, rf_card3])
        else:
            knowledge_list.append([Cluedo.getIdentifierFromNames(playerCharacter, cardShown)])

    return knowledge_list


def accuse(accuser, card1, card2, card3, correct):
    "Construct the CNF clauses representing facts and/or clauses learned from an accusation"
    "*** YOUR CODE HERE ***"
    return_list=[]
    cf_card1 = Cluedo.getIdentifierFromNames('cf',card1)
    cf_card2 = Cluedo.getIdentifierFromNames('cf',card2)
    cf_card3 = Cluedo.getIdentifierFromNames('cf',card3)
    if correct:
        return_list.append([cf_card1])
        return_list.append([cf_card2])
        return_list.append([cf_card3])
        return return_list
    if not correct:
        return_list.append([-cf_card1, -cf_card2, -cf_card3])
        return_list.append([cf_card1, -cf_card2, -cf_card3])
        return_list.append([-cf_card1, cf_card2, -cf_card3])
        return_list.append([-cf_card1, -cf_card2, cf_card3])
        return return_list

